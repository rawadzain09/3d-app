import React, { Fragment } from 'react'
import { OrbitControls } from '@react-three/drei'
import { Canvas } from '@react-three/fiber'

import Atom from './Atom'
import { styled } from 'styled-components'

const Desc = styled.div`
  background-color: white;
  border-radius: 10px;
  width: 200px;
  height: 70px;
  padding: 20px;
  position: absolute;
  top: 400px;
  right: 10px;

  @media only screen and (max-width: 768px) {
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
  }
`
function Development() {
  return (
    <Fragment>
      <Canvas camera={{ position: [0, 0, 10] }}>
        <Atom />

        <OrbitControls enableZoom={false}></OrbitControls>
      </Canvas>
      <Desc>
        We design products with a strong focus on both world class design and
        ensuring your product is a market success.
      </Desc>
    </Fragment>
  )
}

export default Development
