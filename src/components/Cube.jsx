import { PerspectiveCamera, RenderTexture, Text } from '@react-three/drei'
import { useFrame } from '@react-three/fiber'
import { useRef } from 'react'

function Cube() {
  const textRef = useRef()
  useFrame(
    (state) =>
      (textRef.current.position.x = Math.sin(state.clock.elapsedTime) * 2)
  )
  return (
    <mesh>
      <boxGeometry />
      <meshStandardMaterial>
        <RenderTexture attach='map'>
          <PerspectiveCamera
            makeDefault
            position={[0, 0, 5]}
          ></PerspectiveCamera>
          <color attach='background' args={['#dc9dcd']}></color>
          <Text ref={textRef} fontSize={3} color='#555'>
            hello
          </Text>
        </RenderTexture>
      </meshStandardMaterial>
    </mesh>
  )
}

export default Cube
