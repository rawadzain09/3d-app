import React, { Fragment } from 'react'
import { OrbitControls, Stage } from '@react-three/drei'
import { Canvas } from '@react-three/fiber'
import Shoe from './Shoe'
import { styled } from 'styled-components'

const Desc = styled.div`
  background-color: white;
  border-radius: 10px;
  width: 200px;
  height: 70px;
  padding: 20px;
  position: absolute;
  bottom: 40px;
  right: 100px;

  @media only screen and (max-width: 768px) {
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
  }
`
function ProductDesign() {
  return (
    <Fragment>
      <Canvas>
        <Stage environment='city' intensity={0.6}>
          <Shoe />
        </Stage>

        <OrbitControls enableZoom={false}></OrbitControls>
      </Canvas>
      <Desc>
        We design products with a strong focus on both world class design and
        ensuring your product is a market success.
      </Desc>
    </Fragment>
  )
}

export default ProductDesign
